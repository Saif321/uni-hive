<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Review;
use App\Models\DormImage;
use App\Models\ReviewReply;

use Illuminate\Validation\Rule;
class DormController extends Controller
{
    public function add_new_dorm(Request $request){

        // dd($request->images);
        // return $request->images;
        $validated = $request->validate([
            'id' => 'required',
            'description' => 'required',
            'rent_details' => 'required',
            'images' => 'required|array',
            'bedrooms'=>'required|integer',
        ]);
        $dorm =  Dorm::create([
            'dorm_id' => $request->id,
            'rooms' => $request->bedrooms,
            'description' => $request->description,
            'rent_details' => $request->rent_details,
            'lat'=>$request->lat,
            'lng'=>$request->lng,
            'user_id'=>auth()->id(),
        ]);
        // return $dorm;
        $storedFilePath = "";
        if ($dorm) {

          if ($request->hasFile('images')) {
            
            foreach ($request->images as $image) {
                $filename = uniqid() . '.' . $image->getClientOriginalExtension();
                $storedFilePath = Storage::disk('public')->putFileAs('images', $image, $filename);
                DormImage::create([
                    'dorm_id'=>$dorm->id,
                    'image_url'=>$storedFilePath
                    ]);
               
            }
        
          
}


            return response()->json([
                'status' => '200',
                'message' => "created successfully"
            ]);
        }
    }

    public function get_all_dorms(Request $request){
        $dorms = Dorm::with('dorm_images','reviews');
        if($request->dorm_type){
            $dorms->where('rooms',$request->dorm_type);
        }
        return response()->json([
            'status' => '200',
            'dorms' => $dorms->get(),
        ]);
    }

    public function get_specific_dorm(Request $request){
        $validated = $request->validate([
            'dorm_id' => 'required',
        ]);
        $dorm = Dorm::where('id', $request->dorm_id)->with('dorm_images','reviews')->first();
        if ($dorm) {
            return response()->json([
                'status' => '200',
                'dorms' => $dorm,
            ]);
        } else {
            return response()->json([
                'status' => '404',
                'message' => "no dorm found",
            ]);
        }
    }
    
    public function edit_dorm(Request $request,$id){
        
        // if($request->hasfile('image')){
        // $image = $request->file('image');

        // // Generate a unique file name
        // $imageName = time() . '.' . $image->extension();

        // // Move the uploaded image to a public directory
        //  $image->move(public_path('images'), $imageName);
        // }
        // return $request->all();
        // return $request->id;
        $dorm = Dorm::where('id',$id)->update($request->except('image'));
        return response()->json([
            'status'=>200,
            'message'=>"dorm update successfully",
            'dorm'=>$dorm
            ]);
    }
    
    public function delete_dorm(Request $request,$id){
        $dorm = Dorm::where('id',$id)->delete();
        if($dorm){
            return response()->json([
                'status'=>200,
                'message'=>"dorm deleted successfully"
                ]);
        }
    }
    
    public function add_dorm_review(Request $request){
        
        $review = Review::create([
                'dorm_id'=>$request->dorm_id,
                'user_id'=>auth()->id(),
                'review'=>$request->review,
                'rating'=>$request->rating,
            ]);
            
            return response()->json([
                'status'=>'200',
                'message'=>'review submit successfully',
                'review'=>$review
                ]);
        
    }
    
    public function get_dorm_review(Request $request){
        $dorms_reviews = Dorm::where('id',$request->dorm_id)->with('reviews')->get();
        return response()->json([
            'status' => '200',
            'dorm' => $dorms_reviews,
        ]);
    }
    
    public function review_reply(Request $request){
         $validated = $request->validate([
            'review_id' => 'required',
            'reply'=>'required',
        ]);
        
        $reply = ReviewReply::create([
                'review_id' =>  $request->review_id,
                'reply' =>  $request->reply,
                'user_id' =>  auth()->id(),
            ]);
        return response()->json([
            'status'=>200,
            'message' => "reply added successfully",
            'reply' => $reply,
            ]);
    }
    
   
    
}