<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function user(){
        return $this->belongsTo(User::Class, 'user_id','id');
    }
     public function replies(){
        return $this->hasMany(CommentReply::Class, 'comment_id','id')->with('user');
    }
}
