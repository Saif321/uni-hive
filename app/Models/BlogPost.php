<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function comments(){
        return $this->hasMany(BlogComment::class, 'blog_id','id')->with('user','replies');
    }
}
